﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

struct StarTypeP {
	public const float MainSequence = 0.8f;
	public const float Giant = 0.1f;
	public const float Dwarf = 0.098f;
	public const float Neutron = 0.01f;
	public const float BlackHole = 0.01f;
}

public class StarGen : MonoBehaviour {

	public enum StarType {MainSequence=80, Giant=10, Dwarf=8, Neutron=1, BlackHole=1}

	public float radiusScale = 1.5f;
	public float massScale = 10f;
	public float luminosityScale = 1f;
	public int numStars = 10;

	public GameObject starPrefab;
	public Ship ship;

	public GameObject Ship {
		get {
			return ship.gameObject;
		}
		set {
			ship = value.GetComponent<Ship>();
		}
	}

	List<Star> allStars = new List<Star>();

	// Use this for initialization
	void Start () {
		GenerateConstellation(numStars);
		PositionShipInitial();
	}

	void PositionShipInitial() {
		var star = allStars[0];
		//Initial orbit at 3*star radius
		var r = star.radius * 4f;
		var orbitSpeed = Mathf.Sqrt(Star.G * star.mass / r);
		ship.velocity = new Vector3(orbitSpeed, 0, 0);
		ship.transform.position = star.transform.position + new Vector3(0, r, 0);
	}

	void GenerateConstellation(int numStars) {

		for (int i=0; i<numStars; i++) {
			//Generate the stars at zero, then distribute them using the Star Distributor utility
			var star = GenerateStar(new Vector3(0, 0, 0));
			allStars.Add(star);
		}

		var starDist = new PackedCircleDist();
		starDist.Apply(allStars);
	}
		
	Star GenerateStar(Vector3 position){
		//TODO: randomize star type
		var starType = StarType.MainSequence;

		var starObject = (GameObject)Instantiate(starPrefab, position, Quaternion.identity);

		var star = starObject.GetComponent<Star>();
		var temperature = Random.Range(2500f, 20000f);

		MainSequenceStar(temperature, star); //will update star properties
		return star;
	}

	/// <summary>
	/// Main sequence star.
	/// 
	/// Calibrated on the Sun as a standard star with relative
	/// mass, radius, luminosity = 1 at 5780K
	/// Then multiply each with in-game adjustment factors to make them look right...
	/// 
	/// For a main sequence, everything depends directly on temperature
	/// 
	/// Implementation note:
	/// Because we will generate stars by copying a hidden star 'prototype' so we can assemble
	/// the gameobject in the scene first, the method sets the properties on the given gameobject
	/// rather than returning them...
	/// 
	/// </summary>
	/// <param name="temperature">Temperature.</param>
	void MainSequenceStar(float temperature, Star star){
		//Star Properties:
		//Temperature (color)
		//Radius
		//Luminosity

		//TODO: want something analogous to main sequence star, as in hertzsprung-russel diagram


		//From spreadsheet looking at relation between star classes, temperature, mass, radius for
		//main sequence stars, calibrated on Sun at 5780K with relative radius 1(ish)

		var radius = radiusScale * Mathf.Pow(temperature/5780f, 0.9f);

		//Relative Mass = RelT^(4/2.5) http://physics.stackexchange.com/questions/6771/star-surface-temperature-vs-mass
		var mass = massScale * Mathf.Pow(temperature/5780f, 4f/2.5f);

		//Relative lum = R^2 * T^4 http://www.idialstars.com/april2013.htm
		//Note: non-scaled radius=1.0 for reference star, so relative radius is scaled radius/scale factor
		var lum = luminosityScale * Mathf.Pow(temperature/5780f, 4) * Mathf.Pow(radius/radiusScale, 2);


		star.temperature = temperature;
		star.mass = mass;
		star.radius = radius;
		star.luminosity = lum;
		star.type = StarType.MainSequence;

		star.Start(); // refresh the star's internal constructor...
	}

}
