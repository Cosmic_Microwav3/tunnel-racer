﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Define properties and behaviour of Stars
/// 
/// Note: relation between star mass and sphere of influence
/// 
/// Actually want SOI to be based on the mass, such that the force
/// at the edge of the SOI is 'small'
/// 
/// </summary>
public class Star : MonoBehaviour {

	public const float G = 1f;

	/// <summary>
	/// The acceleration we want at the limit of the SOI
	/// so that we define the SOI as a function of Mass using
	/// r = Sqrt(G * M / limAccel)
	/// </summary>
	const float LIM_ACCEL = 0.02f;

	public StarGen.StarType type;

    // crash radius
	public float radius;
	public float mass;
	public float temperature = 5000f;
	public float luminosity = 0;

	private float _sphereOfInfluenceRadius;

	public float SphereOfInfluenceRadius {
		get {
			return _sphereOfInfluenceRadius;
		}
	}

	Color color;
	LineRenderer soiRenderer;

	float baseIntensity = 0;
		
	// Use this for initialization
	public void Start () {
		soiRenderer = gameObject.GetComponent<LineRenderer>();

		var collider = GetComponent<CircleCollider2D>();

		color = ColorTemp.ColorFromTemperature(temperature);

		_sphereOfInfluenceRadius = Mathf.Sqrt(G * mass / LIM_ACCEL);
		collider.radius = _sphereOfInfluenceRadius;


		SetVisuals();
	}

	// Update is called once per frame
	void Update () {
		UpdateVisuals();
	}

	/// <summary>
	/// Calculates the acceleration due to gravity of the object at the given position
	/// Note that to get the FORCE you need to further multiply by the orbiter's mass
	/// </summary>
	/// <returns>The acceleration.</returns>
	/// <param name="orbiterPos">Orbiter position.</param>
	public Vector3 GravityAcceleration(Vector3 orbiterPos){
		var delta = transform.position - orbiterPos ;
		var accel = delta.normalized  * mass / delta.sqrMagnitude;
		return accel;
	}

	/// <summary>
	/// Sets the visual properties of the child objects that consitute
	/// the star display
	/// </summary>
	void SetVisuals() {
		var light = GetComponentInChildren<Light>();
		light.color = color;
		light.range = 1.2f * _sphereOfInfluenceRadius;

		//base light intensity is 0.5
		//light scales from 0-8
		//rel lum increases from 1 to 1000
		//vary light intensity with log lum, conveniently Ln(1000) = 6ish
		//so just set rel lum for sun = 2, 
		baseIntensity = 2 + Mathf.Log(luminosity);
		light.intensity = baseIntensity;

//		var shader = GetComponentInChildren<StarMatter>();
		var mesh = transform.FindChild("StarMesh");
		mesh.localScale = new Vector3(radius, radius, 1);

		var material = mesh.GetComponent<Renderer>().material;

		material.SetColor("_Color", color);
		material.SetColor("_EmissionColor", color * 0.8f);

		soiRenderer.SetVertexCount(24);
		DrawSOI();
			
	}

	void UpdateVisuals(){
		var light = GetComponentInChildren<Light>();
		// oscillate the light intensity by a fraction of it's magnitude (only want about 10% variation)
		light.intensity = baseIntensity + 0.2f * baseIntensity * (1+Mathf.Sin(Time.time))	;
			
	}

	void DrawSOI(){
		soiRenderer.SetVertexCount(50);

		var dTheta = 2*Mathf.PI / 49;
		var theta = 0f;
		float x,y;
		for (int i = 0; i <= 49; i++){
			x = _sphereOfInfluenceRadius * Mathf.Cos(theta) + transform.position.x;
			y = _sphereOfInfluenceRadius * Mathf.Sin(theta) + transform.position.y;
			soiRenderer.SetPosition(i, new Vector3(x, y, 0));

			theta += dTheta;
		}
	}

	void OnDisable() {
		//TODO: find a way to clear currentStar if that star becomes disabled
	}

	void OnGui(){
		DrawSOI();
	}

	void OnTriggerEnter2D(Collider2D other) {

	}

	void OnTriggerStay2D(Collider2D other) {

	}

	void OnTriggerExit2D(Collider2D other) {
	}

	public override string ToString(){
		return "Temperature " + temperature + "Position: " + transform.position.ToString();
	}













	/* ------------------------------------------------------------------------------------
	 * Exact orbital calculations, based on analytic solutions of Kepler's laws
	 * TODO: these are either buggy or not well understood. they give roughly reasonable
	 * looking trajectories that don't properly line up with the numerical solution ones...
	 **/

	/// <summary>
	/// Get orbital properties for a body in this Star's SOI
	/// at radius r and velocity v
	/// http://space.stackexchange.com/questions/1904/how-to-programmatically-calculate-orbital-elements-using-position-velocity-vecto/1919#1919
	/// 
	/// Return:
	/// a is semi-major axis (distance) half the "long width" of the ellipse, passing through both foci
	/// p is "semi-latus rectum" ie. half the "narrow width" of the ellipse at the foci
	/// i is elevation angle relative to the orbital plane
	/// Omega is longitude of ascending node
	/// argp = small omega = argument of perigee (angle of the perigee)
	/// nu is ???
	/// e is the eccentricity
	/// </summary>
	/// <param name="r">The red component.</param>
	/// <param name="v">V.</param>
	public float[] Orbital(Vector3 r, Vector3 v) {

		var mu = mass * G;
		var h = Vector3.Cross(r, v); // Angular momentum
		var nhat = Vector3.Cross(Vector3.forward, h);
		var evec = ((v.sqrMagnitude - mu / r.magnitude) * r - Vector3.Dot(r, v) * v) / mu;
		var e = (float) evec.magnitude;
		var energy = v.sqrMagnitude / 2 - mu / r.magnitude;

		//a is semi-major axis (distance)
		//p is "semi-latus rectum" ie. half the "narrow width" of the ellipse at the foci 
		float a, p, i, Omega, argp, nu;

		//if eccentricity e <= 1, can define a and p, else we have 'open curve' and a in infinite
		if (Math.Abs(e - 1.0) > Mathf.Epsilon) {
			a = - mu / (2 * energy);
			p = a * (1.0f - Mathf.Pow(e, 2));
		}
		else {
			a = Mathf.Infinity;
			p = h.sqrMagnitude / mu;
		}

		// i is elevation of orbital plane
		// since we look from behind (-z) is going to be 0 or 180 (on axis) i.e. Pi in radians
		i = Mathf.Acos(h.z / h.magnitude);

		//Omega is longitude of ascending node
		Omega = Mathf.Acos(nhat.x / nhat.magnitude);

		if (nhat.y < 0) {
			Omega = 360 - Omega;
		}

		//argp = small omega = argument of perigee (angle of the perigee)
		argp = Mathf.Acos(Vector3.Dot(nhat, evec) / nhat.magnitude * e);

		if (evec.z < 0) {
			argp = 360 - argp;
		}

		nu = Mathf.Acos(Vector3.Dot(evec, r)/(e * r.magnitude));

		if (Vector3.Dot(r, v) < 0 ){
			nu = 360 - nu;
		}

		//Results:
		//a is semi-major axis (distance)
		//p is "semi-latus rectum" ie. half the "narrow width" of the ellipse at the foci
		//i is elevation of orbital plane
		//Omega is longitude of ascending node
		//argp = small omega = argument of perigee (angle of the perigee)
		//nu is
		//e is the eccentricity
		return new [] {a, p, i, Omega, argp, nu, e};
	}

	float EccentricAnomaly(float x, float a) {
		return Mathf.Acos(x / a);
	}


	float TimeOrbital(Vector3 r_vec, Vector3 v){
		var orbit_props = Orbital(r_vec, v);

		var a = orbit_props[0];
//		var p = orbit_props[1];
//		var i = orbit_props[2];
//		var Omega = orbit_props[3];
//		var argp = orbit_props[4];
//		var nu = orbit_props[5];
		var e = orbit_props[6];

		//eccentricAnomaly
		var eccentricAnomaly = Mathf.Acos(r_vec.x / a);

		var meanAnomanly = eccentricAnomaly - e * Mathf.Sin(eccentricAnomaly);
		return meanAnomanly;
	}

	float OrbitPeriod(Vector3 r_vec, Vector3 v){
		//{\frac {T^{2}}{a^{3}}}={\frac {4\pi ^{2}}{G(M+m)}} 
		var orbit_props = Orbital(r_vec, v);

		var a = orbit_props[0];

		//Assume star mass >> orbiter mass -> M + m ~= M
		var T = Mathf.Sqrt(Mathf.Pow(a,3) *4 * Mathf.Pow(Mathf.PI, 2) /(G * mass));
		return T;
	}

	public List<Vector3> Trajectory(Vector3 r_vec, Vector3 v){
		//r vec is the vector from body centre to ship centre

		var orbit_props = Orbital(r_vec, v);

//		var a = orbit_props[0];
		var p = orbit_props[1];
//		var i = orbit_props[2];
//		var Omega = orbit_props[3];
//		var argp = orbit_props[4];
//		var nu = orbit_props[5];
		var e = orbit_props[6];

		float r, x, y;
		float theta;

		var trajectory = new List<Vector3>();

		//Given a closed (elliptical) trajectory, just need to spin 360˚
		if (Math.Abs(e - 1.0) > Mathf.Epsilon) {
			for (theta = 0; theta <= 2 * Math.PI; theta += 0.05f) {
				r = p / (1 + e * Mathf.Cos(theta));
				x = r * Mathf.Cos(theta);
				y = r * Mathf.Sin(theta);
				trajectory.Add(new Vector3(x, y, 0));
			}
		}
		else {
			//If the path is para/hyperbolic, need to set limit on max radius to draw.
			//Start with current position vector and draw until r >= sphere of influence radius

			var ship_angle = Mathf.Atan2(r_vec.y, r_vec.x);

			var d_theta = Mathf.PI/360; // step of 0.5degree
			theta = ship_angle;

			r = r_vec.magnitude;

			while (r <= _sphereOfInfluenceRadius) {
				r = p / (1 + e * Mathf.Cos(theta));
				x = r * Mathf.Cos(theta);
				y = r * Mathf.Sin(theta);
				theta += d_theta;
				trajectory.Add(new Vector3(x, y, 0f));
			}
		}

		return trajectory;

	}


}