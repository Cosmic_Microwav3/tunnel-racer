﻿Shader "Unlit/VertsAsSpheres"
{
	Properties
	{
//		_MainTex ("Texture", 2D) = "white" {}
		_VertColor ("Vert colour", Color) = (1, 0, 0, 1)    // (R, G, B, A)
		_LightDir ("Light Dir", Vector) = (0, 0, 1) // (x, y, z, w)
	}
	SubShader
	{
		Tags {
			"Queue" = "Transparent"
        	"RenderType" = "Transparent"
		}

		Pass
		{
			CGPROGRAM
		    #pragma vertex vert             
		    #pragma fragment frag

			#include "UnityCG.cginc"
//			sampler2D _MainTex;
//			float4 _MainTex_ST;
			float4 _VertColor;
			float3 _LightDir;

		    struct vertInput {
		        float4 pos : POSITION;
		    };  

		    struct vertOutput {
		        float4 pos : SV_POSITION;
		    };

		    vertOutput vert(vertInput input) {
		        vertOutput o;
		        o.pos = mul(UNITY_MATRIX_MVP, input.pos);
		        return o;
		    }

		    half4 frag(vertOutput output) : COLOR {

			    float3 N;
			    N.xy = output.pos * 2.0 - float2(1.0, 1.0);    
			    float mag = dot(N.xy, N.xy);
//			    if (mag > 10.0) discard;   // kill pixels outside circle
			    N.z = sqrt(1.0-mag);

			    // calculate lighting
	//			    float diffuseVal = max(0.0, dot(_LightDir, N));
			    float diffuseVal = 1.0;
			    fixed4 col = _VertColor * diffuseVal;
				return col;
//
//		        return half4(1.0, 0.0, 0.0, 1.0); 
		    }


//			// make fog work
////			#pragma multi_compile_fog
//			
//

//
//			struct vertInput
//			{
//				float2 uv : TEXCOORD0;
//				float4 vertex : SV_POSITION;
//			};
//

//
//			vertInput vert (appdata v)
//			{
//				vertInput o;
//				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
////				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
//				return o;
//			}
//			
//			fixed4 frag (vertInput i) : SV_Target
//			{
//				// sample the texture
////				fixed4 col = tex2D(_MainTex, i.uv);
//				// apply fog
////				UNITY_APPLY_FOG(i.fogCoord, col);
//
//				//http://mmmovania.blogspot.de/2011/01/point-sprites-as-spheres-in-opengl33.html
//
//				// calculate normal from texture coordinates
//				float3 N;
//			    N.xy = i.vertex * 2.0 - float2(1.0, 1.0);    
//			    float mag = dot(N.xy, N.xy);
//			    if (mag > 1.0) discard;   // kill pixels outside circle
//			    N.z = sqrt(1.0-mag);
//
//			    // calculate lighting
////			    float diffuseVal = max(0.0, dot(_LightDir, N));
//			    float diffuseVal = 1.0;
//			    fixed4 col = _VertColor * diffuseVal;
//				return col;
//
//				//float Ns = 250;
//				//vec4 mat_specular=vec4(1); 
//				//vec4 light_specular=vec4(1); 
//				//void main(void)
//				//{
//				//    // calculate normal from texture coordinates
//				//    vec3 N;
//				//    N.xy = gl_PointCoord* 2.0 - vec2(1.0);    
//				//    float mag = dot(N.xy, N.xy);
//				//    if (mag > 1.0) discard;   // kill pixels outside circle
//				//    N.z = sqrt(1.0-mag);
//				//
//				//    // calculate lighting
//				//    float diffuse = max(0.0, dot(lightDir, N));
//				// 
//				//    vec3 eye = vec3 (0.0, 0.0, 1.0);
//				//    vec3 halfVector = normalize( eye + lightDir);
//				//    float spec = max( pow(dot(N,halfVector), Ns), 0.); 
//				//    vec4 S = light_specular*mat_specular* spec;
//				//    vFragColor = vec4(Color,1) * diffuse + S;
//				//}
//			}
			ENDCG
		}
	}
}
