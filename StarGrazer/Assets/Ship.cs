﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


struct State
{
	public Vector3 pos;      // position
	public Vector3 v;      // velocity
}

struct Derivative
{
	public Vector3 dx;      // dx/dt = velocity
	public Vector3 dv;      // dv/dt = acceleration
}

public class Ship : MonoBehaviour {
	public Text healthDisplay;
	public Text fuelDisplay;
	public Text speedDisplay;


	public float predictTime = 20.0f;

	public float engineThrust = 0.4f;
	public float rotationForce = 1.1f; // the power of rotation thrusters
	public float rotationDamp = 0.04f;
	public float fuel = 100;
	public float health = 100;

	/// <summary>
	/// Damage multiplier as a function of "temperature" for the most vulnerable
	/// part of the ship (without sheilding) in health lost/degree
	/// </summary>
	public float damageMultipier = 0.005f; //lose 0.5 health per 100Kelvin

	/// Fuel gain rate as a function of "temperature" (just a way of quantifying star power)
	/// in fuel units/degree
	public float fuelGainRate = 0.01f; // 1 unit gain per 100K

	/// <summary>
	/// The fuel use rate in units/second when the engine is on
	/// </summary>
	public float fuelUseRate = 1f;

	/// <summary>
	/// The base fuel use rate. Makes it possible for the ship
	/// to continuously consume fuel (for life support, repairs)
	/// while cruising without power...
	/// </summary>
	public float fuelBaseRate = 0f;

	/// <summary>
	/// The velocity.
	/// </summary>
	public Vector3 velocity;

	Vector3 thrust;
	Vector3 acceleration;

	float rotationSpeed = 0;
	float damageThreshold = 3;
	float fuelThreshold = 3;
	// actually doesn't matter just don't want to accellerate indefinately
//	float maxrotationSpeed = 3000f; 

//	public float startSpeed = 0.1f;
	Star currentStar;

	private LineRenderer trajectoryRenderer;


	void Start () {
		trajectoryRenderer = gameObject.GetComponent<LineRenderer>();
		healthDisplay.text = String.Format("Health: {0:###}", health);
		fuelDisplay.text = String.Format("Fuel: {0:###}", fuel);
		speedDisplay.text = String.Format("Speed: {0:###}", 0);

	}
	
	void Update () {
		var dt = Time.deltaTime;

		ThrustInput(dt);

		SteerInput(dt);

		if (currentStar != null) {
			StarBurn(currentStar, dt);
			StarFuel(currentStar, dt);
		}

		PositionUpdate(dt);

		healthDisplay.text = String.Format("Health: {0:###}", health);
		fuelDisplay.text = String.Format("Fuel: {0:###}", fuel);
		speedDisplay.text = String.Format("Speed: {0:F}", velocity.magnitude);

	}

	/// <summary>
	/// Apply thrust input and calculate fuel use
	/// </summary>
	void ThrustInput(float dt) {
		//First see if we should do velocity adjustments...
		//Set the thrust vector here, which is later used in the update function
		if (Input.GetAxis("RocketOn") == 1) {
			transform.Find("Engine").GetComponent<EllipsoidParticleEmitter>().emit = true;
			thrust = transform.right * engineThrust;

			fuel -= fuelUseRate * dt;
		}
		else {
			transform.Find("Engine").GetComponent<EllipsoidParticleEmitter>().emit = false;
			thrust = Vector3.zero;
		}

		fuel -= fuelBaseRate * dt;
	}

	/// <summary>
	/// Handle the ship rotation input
	/// </summary>
	void SteerInput(float dt){
		var steerAmount = Input.GetAxis("Rotate");
		//TODO better input response
		if (steerAmount > 0){
			rotationSpeed += rotationForce * dt;
		}
		else if (steerAmount < 0){
			rotationSpeed -= rotationForce * dt;
		}
		else {
			//if no input, damp the rotation
			//exponential decay...
			var dv = - rotationDamp * rotationSpeed;

			rotationSpeed += dv;
		}

		//apply rotation
		transform.Rotate(Vector3.forward * rotationSpeed);
	}


	/// <summary>
	/// Update the position and velocity vectors as a function of
	/// Gravity forces and current thrust
	/// </summary>
	/// <param name="dt">Dt.</param>
	void PositionUpdate(float dt){
		var state = new State();
		state.pos = transform.position;
		state.v = velocity;

		var newState = integrate(state, dt, false);
		transform.position = newState.pos;
		velocity = newState.v;
	}

	/// 
	/// RK4 integrator for determining ship state update
	/// 
	/// Since gravity is a fairly 'delicate' force and we want accurate simulation,
	/// as well as to predict the trajectory quite for (10s of seconds) into the future,
	/// need to use the more comlicated + accurate RK4 integrator instead of Euler
	/// integration.
	/// 
	/// NOTE TODO: Could also try to use the exact solutions of Kepler's laws, but that
	/// has proven difficult so far...
	/// 
	/// state : a State struct containing the positiona and velocity
	/// dt : the time delta
	/// gravityOnly: hack to allow the integrate function to be used both to update
	/// 			 ship position and plot trajectories. When the ship is under thrust,
	/// 			 the thrust force is much larger than the gravity force, which results in a 
	/// 			 weird looking trajectory if you base it on the current acceleration. 
	/// 			 Therefore, only consider the gravitational forces when rendering trajectory.
	State integrate(State state, float dt, bool gravityOnly)
	{
		Derivative a,b,c,d;

		a = evaluate( state, 0.0f, new Derivative(), gravityOnly);
		b = evaluate( state, dt*0.5f, a, gravityOnly);
		c = evaluate( state, dt*0.5f, b, gravityOnly );
		d = evaluate( state, dt, c, gravityOnly);

		Vector3 dxdt = 1.0f / 6.0f * 
			( a.dx + 2.0f*(b.dx + c.dx) + d.dx );

		Vector3 dvdt = 1.0f / 6.0f * 
			( a.dv + 2.0f*(b.dv + c.dv) + d.dv );

		//TODO: perhaps juse use the position and velocity mutables directly...
		var output = new State();
		output.pos = state.pos + dxdt * dt;
		output.v = state.v + dvdt * dt;
		return output;
	}

	Vector3 GravityAcceleration(State state){
		if (currentStar != null) {
//			var star = currentStar.GetComponent<Star>();
			return currentStar.GravityAcceleration(state.pos);
		}
		else {
			return Vector3.zero;
		}
	}

	Vector3 SumAcceleration(State state){
		return GravityAcceleration(state) + thrust;
	}

	/// <summary>
	/// Evaluate the derivative of the update funciton for the
	/// specified initial, dt, d and gravityOnly.
	/// </summary>
	/// <param name="initial">Initial.</param>
	/// <param name="dt">Dt.</param>
	/// <param name="d">D.</param>
	/// <param name="gravityOnly">If set to <c>true</c> gravity only.</param>
	Derivative evaluate(State initial, float dt, Derivative d, bool gravityOnly)
	{
		State state;
		state.pos = initial.pos + d.dx * dt;
		state.v = initial.v + d.dv * dt;

		Derivative output;
		output.dx = state.v;
		if (gravityOnly) {
			output.dv = GravityAcceleration(state);
		}
		else {
			output.dv = SumAcceleration(state);	
		}
		return output;
	}


//	List<Vector3> CalculateTrajectory(GameObject star) {
//		// Calculate the future trajectory.
//		// need to do some fiddling so we don't draw over outselves...
//
//		var starScript = star.GetComponent<Star>();
//
//		var delta = transform.position - star.transform.position;
//		var vel = new Vector3(rb.velocity.x, rb.velocity.y, 0);
//		return starScript.Trajectory(delta, vel);
//
//	}
	List<Vector3> CalculateTrajectory(Star star) {
		// Calculate the future trajectory.
		// need to do some fiddling so we don't draw over outselves...

		var starPos = star.transform.position;

		var shipPos = transform.position;

		var results = new List<Vector3>();

		Vector3 vel, delta;

		vel = velocity;

		//Adaptive DT by defining constant seconds^2/meter instead of constant seconds
		//means that you get denser DT around high-velocity points
		float dt;
		// when the velocity is really small want to set a minimum dt
		if (vel.magnitude < 2){
			dt = 0.05f;
		}
		else {
			dt = 1 / (10 * vel.magnitude);
			if (dt < 0.002 ){
				dt = 0.002f;
			}
		}

		float time = 0;

		int maxVerts = 1000;

		while (time < predictTime) {
			delta = shipPos - starPos;

			if (delta.magnitude < star.radius) {
				break;
			}
			else if (delta.magnitude > star.SphereOfInfluenceRadius){
				break;
			}

			//TODO: here could use the better integrator...
//			accel = starScript.GravityAcceleration(shipPos);
//			vel = vel + accel * dt;
//			shipPos = shipPos + vel * dt;

			var state = new State();
			state.pos = shipPos;
			state.v = vel;

			var newState = integrate(state, dt, true);
			shipPos = newState.pos;
			vel = newState.v;

			results.Add(shipPos);
			if (vel.magnitude < 2){
				dt = 0.05f;
			}
			else {
				dt = 1 / (10 * vel.magnitude);
				if (dt < 0.001 ){
					dt = 0.001f;
				}
			}			
			time += dt;
			if (results.Count > maxVerts){
				break;
			}
		}

		return results;
	}

	/// <summary>
	/// Calculate the fuel gain and health loss due to proximity to star
	/// </summary>
	/// <param name="starObject">Star object.</param>
	/// <param name="dt">Dt.</param>
	void StarBurn(Star star, float dt)
	{
		var delta = star.transform.position - transform.position;
		var r = delta.magnitude;
		if (r < star.radius ){
			//die! For now, just freeze the game
			Time.timeScale = 0.0f;
		}

//		var toSurface = r - star.radius;
		//fuel and damage related to star temperature
		//scaled r2 falloff, where the temperature is the star temperature at the surface
		var scaledTemp = star.temperature * Mathf.Pow(r - (star.radius -1), -2);

		// Also need to be pretty close to take damage,
		// but not necessarily the same distance (start taking damage before fuel?)
		if (r < damageThreshold * star.radius ) {
			// damage related to temperature but also to orientation
			// TODO also should scale more sharply, giving you a fine window between
			// being close enough to pick up fuel and but not so close you burn up

			// Ship points "right"
			// Angle of front of ship relative to star in degrees
			var shipAngle =  Vector3.Angle(delta, transform.right);

			// Sheild protection max head on, tails off either side
			// use a function that peaks there and tails off, e.g. Cos(x)
			// Also need to scale damage appropriately...
			var damage = damageMultipier * scaledTemp *  (1 - Mathf.Cos(Mathf.Deg2Rad *shipAngle));
			health -= damage * dt;
		}
	}

	/// <summary>
	/// Fuel from the Stars.
	/// </summary>
	/// <param name="starObject">Star object.</param>
	/// <param name="dt">Dt.</param>
	void StarFuel(Star star, float dt) {
		var delta = star.transform.position - transform.position;
		var r = delta.magnitude;

		var toSurface = r - star.radius;

		//fuel and damage related to star temperature
		//scaled exponential falloff, where the temperature is the star temperature at the surface
		var scaledTemp = star.temperature * Mathf.Pow(r - (star.radius -1), -1);

		// need to be pretty close to start collecting fuel
		if (r < fuelThreshold * star.radius ) {
			var fuelGain = fuelGainRate * scaledTemp * dt;
			fuel += fuelGain;
		}
	}

	List<Vector3> Orbit(GameObject star) {
		var starScript = star.GetComponent<Star>();
		var delta = transform.position - star.transform.position;
		var orbit = starScript.Trajectory(delta, velocity);
		return orbit;
	}

	/// <summary>
	/// Draws the projected trajectory of the ship.
	/// Gets the predicted trajectory and samples it at a lower
	/// resolution to produce the line
	/// </summary>
	/// <param name="star">Star.</param>
	void DrawTrajectory(Star star){
		List<Vector3> trajectory = CalculateTrajectory(star);
		//		List<Vector3> trajectory = Orbit(star);

		int nStep = 4;
		int renderCount = trajectory.Count / nStep;
		trajectoryRenderer.SetVertexCount(renderCount);

		// for use with orbit equations...
		//		trajectoryRenderer.SetVertexCount(trajectory.Count);
		//
		//		for (int i = 0; i < trajectory.Count; i++){
		//			trajectoryRenderer.SetPosition(i, trajectory[i]);
		//		}

		int i = 0;
		int j = 0;
		while ( i < renderCount && j < trajectory.Count){
			trajectoryRenderer.SetPosition(i, new Vector3(trajectory[j].x, trajectory[j].y, 0));
			i += 1;
			j += nStep;
		}
	}

	void OnGui(){
		if (currentStar != null) {
			DrawTrajectory(currentStar);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		//TODO what happens when gravity fields overlap?
		var star = other.GetComponent<Star>();
		if (star != null){
			currentStar = star;
			trajectoryRenderer.enabled = true;			
		}

	}

	void OnTriggerStay2D(Collider2D other) {
		if (currentStar != null) {
			DrawTrajectory(currentStar);
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		currentStar = null;
		trajectoryRenderer.enabled = false;
	}


}
