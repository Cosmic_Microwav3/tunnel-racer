﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CreateNoise))]
public class CreateNoiseEditor : Editor 
{
    private CreateNoise Target;

    void OnEnable()
    {
        Target = (CreateNoise)target;
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Create"))
        {
            Texture3D tex = Target.GenerateTexture();
            AssetDatabase.CreateAsset(tex, Target.texturePath);
            AssetDatabase.Refresh();
        }

        DrawDefaultInspector();
    }
}
