﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CombineTexture3D))]
public class CombineTexture3DEditor : Editor 
{
    private CombineTexture3D Target;

    void OnEnable()
    {
        Target = (CombineTexture3D)target;
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Combine"))
        {
            if (!Target.inputTexture1)
            {
                Target.inputTexture1 = (Texture3D)AssetDatabase.LoadAssetAtPath(Target.pathTexture1, typeof(Texture3D));
            }

            if (!Target.inputTexture2)
            {
                Target.inputTexture2 = (Texture3D)AssetDatabase.LoadAssetAtPath(Target.pathTexture2, typeof(Texture3D));
            }

            Texture3D tex = Target.Combine(Target.inputTexture1, Target.inputTexture2);
            AssetDatabase.CreateAsset(tex, Target.outputTexturePath);
            AssetDatabase.Refresh();
        }

        DrawDefaultInspector();
    }
}
