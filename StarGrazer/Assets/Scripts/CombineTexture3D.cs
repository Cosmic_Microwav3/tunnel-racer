﻿using UnityEngine;

public class CombineTexture3D : MonoBehaviour 
{
    public string outputTexturePath = "Assets/";
    public int width = 256;
    public int height = 256;
    public int depth = 64;

    public enum BlendType
    {
        Normal50,
        Multiply,
        NegativeMultiply,
        Overlay
    }

    public BlendType blendMode = BlendType.Normal50;

    public string pathTexture1 = "Assets/";
    public Texture3D inputTexture1;
    public float scale1_w = 1f;
    public float scale1_h = 1f;
    public float scale1_d = 1f;

    public string pathTexture2 = "Assets/";
    public Texture3D inputTexture2;
    public float scale2_w = 1f;
    public float scale2_h = 1f;
    public float scale2_d = 1f;

    private Texture3D output;

    public Texture3D Combine(Texture3D tex1, Texture3D tex2)
    {
        output = new Texture3D(width, height, depth, TextureFormat.RGBA32, false);

        Color[] colors1 = tex1.GetPixels();
        int w1 = tex1.width;
        int h1 = tex1.height;
        int d1 = tex1.depth;

        Color[] colors2 = tex2.GetPixels();
        int w2 = tex2.width;
        int h2 = tex2.height;
        int d2 = tex2.depth;

        Color[] colors = new Color[width * height * depth];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                for (int z = 0; z < depth; z++)
                {
                    int idx1 = ((int)(x / scale1_w) % w1) + ((int)(y / scale1_h) % h1) * w1 + ((int)(z / scale1_d) % d1) * w1 * h1;
                    int idx2 = ((int)(x / scale2_w) % w2) + ((int)(y / scale2_h) % h2) * w2 + ((int)(z / scale2_d) % d2) * w2 * h2;

                    Color c = Color.white;
                    switch (blendMode)
                    {
                        case BlendType.Normal50:
                            c = (colors1[idx1] + colors2[idx2]) / 2f;
                            break;
                        case BlendType.Multiply:
                            c = colors1[idx1] * colors2[idx2];
                            break;
                        case BlendType.NegativeMultiply:
                            c = Color.white - (Color.white - colors1[idx1]) * (Color.white - colors2[idx2]);
                            break;
                        case BlendType.Overlay:
                            c = new Color(
                                    (colors2[idx2].r < 0.5) ? 2 * colors1[idx1].r * colors2[idx2].r : 1 - 2 * (1 - colors1[idx1].r) * (1 - colors2[idx2].r),
                                    (colors2[idx2].g < 0.5) ? 2 * colors1[idx1].g * colors2[idx2].g : 1 - 2 * (1 - colors1[idx1].g) * (1 - colors2[idx2].g),
                                    (colors2[idx2].b < 0.5) ? 2 * colors1[idx1].b * colors2[idx2].b : 1 - 2 * (1 - colors1[idx1].b) * (1 - colors2[idx2].b),
                                    (colors2[idx2].a < 0.5) ? 2 * colors1[idx1].a * colors2[idx2].a : 1 - 2 * (1 - colors1[idx1].a) * (1 - colors2[idx2].a)
                                );
                            break;
                        default:
                            break;
                    }
                    colors[x + y * width + z * width * height] = c;
                }
            }
        }

        output.SetPixels(colors);
        output.Apply();
        return output;
    }
}
