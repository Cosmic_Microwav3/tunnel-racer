﻿using UnityEngine;

public class CreateNoise : MonoBehaviour 
{
    public string texturePath = "Assets/";

    public int width = 256;
    public int height = 256;
    public int depth = 64;

    public float scale_w = 1f;
    public float scale_h = 1f;
    public float scale_d = 1f;

    public enum NoiseType
    {
        ValueNoise,
        SimplexNoise
    }

    public NoiseType noiseType = NoiseType.SimplexNoise;

    public float smallestFrequency = 0.01f;
    public float maxAmplitude = 0.5f;
    public int octaves = 8;
    public float lacunarity = 2.0f;
    public float gain = 0.5f;
    public bool renormalize = true;

    private Texture3D tex;

    public Texture3D GenerateTexture()
    {
        // Create a tempory 3D Texture
        tex = new Texture3D(width, height, depth, TextureFormat.RGBA32, false);

        // We need an array for the intensity of size width * height * depth
        float[] vals = new float[width * height * depth];
        // The multi-octave noise will might have a minimum significantly larger than zero and smaller than one. 
        // These here are temp variables for the actual minimum and maximum values.
        float maxval = 0;
        float minval = 1;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                for (int z = 0; z < depth; z++)
                {
                    // Start with the smallest frequency and largest amplitude
                    float freq = smallestFrequency;
                    float amplitude = maxAmplitude;

                    // Generates the noise for multiple octaves
                    float val = 0f;
                    for (int i = 0; i < octaves; i++)
                    {
                        // the noise is in the range if [-1,1], convert this to [0, 1]
                        val += 0.5f * (amplitude * GenerateRepeatingNoise(x / scale_w, y / scale_h, z / scale_d, freq, width / scale_w, height / scale_h, depth / scale_d) + 1f);

                        // each octave has a frequency larger by a factor of the lacunarity
                        freq *= lacunarity;
                        // and deminished in amplitude by the gain (if gain < 1)
                        amplitude *= gain;
                    }

                    // if this value exceeds the bounds, save it as the new maximum/minimum
                    if (val > maxval) maxval = val;
                    if (val < minval) minval = val;

                    // save value into array
                    vals[x + y * width + z * width * height] = val;
                }
            }
        }

        // convert values into grayscale colors
        Color[] colors = new Color[width * height * depth];

        for (int i = 0; i < vals.Length; i++)
        {
            // if necessary, renormalize the value first
            float c = (renormalize) ? (vals[i] - minval) / (maxval - minval) : vals[i];
            colors[i] = new Color(c, c, c, c);
        }

        tex.SetPixels(colors);
        tex.Apply();

        return tex;
    }

    private float GenerateRepeatingNoise(float x, float y, float z, float freq, float w, float h, float d)
    {
        System.Func<Vector3, float, NoiseSample> noiseFunc = null;
        switch (noiseType)
        {
            case NoiseType.SimplexNoise:
                noiseFunc = Noise.Simplex3D;
                break;
            case NoiseType.ValueNoise:
                noiseFunc = Noise.SimplexValue3D;
                break;
            default:
                break;
        }

        // this convoluted expression makes sure that the noise tiles nicely in all directions
        float val = (d - z) * (h - y) * (w - x) * noiseFunc(new Vector3(x, y, z), freq).value + (d - z) * (h - y) * x * noiseFunc(new Vector3(x - w, y, z), freq).value
            + (d - z) * y * (w - x) * noiseFunc(new Vector3(x, y - h, z), freq).value + (d - z) * y * x * noiseFunc(new Vector3(x - w, y - h, z), freq).value
            + z * (h - y) * (w - x) * noiseFunc(new Vector3(x, y, z - d), freq).value + z * (h - y) * x * noiseFunc(new Vector3(x - w, y, z - d), freq).value
            + z * y * (w - x) * noiseFunc(new Vector3(x, y - h, z - d), freq).value + z * y * x * noiseFunc(new Vector3(x - w, y - h, z - d), freq).value;

        val /= (float)(w * h * d);

        return val;
    }
}
