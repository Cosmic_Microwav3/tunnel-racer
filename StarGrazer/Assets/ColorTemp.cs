﻿/**
* Ported to C# by Jonathan Chambers from npm module:
*  
*  color-temperature.js
*
*  Neil Bartlett
*  neilbartlett.com
*  2015-01-22
*
*  Copyright [2015] [Neil Bartlett] *
*
* Color Temperature is the color due to black body radiation at a given
* temperature. The temperature is given in Kelvin. The concept is widely used
* in photography and in tools such as f.lux.
*
* The function here converts a given color temperature into a near equivalent
* in the RGB colorspace. The function is based on a curve fit on standard sparse
* set of Kelvin to RGB mappings.
*
* Two conversions are presented here. The one colorTempertature2RGBUSingTH
* is a JS version of the algorithm developed by Tanner Helland. The second is a
* slightly more accurate conversion based on a refitting of the original data
* using different curve fit functions. The performance cost of the two
* approaches is very similar and in general the second algorithm is preferred.
*
* NOTE The approximations used are suitable for photo-mainpulation and other
* non-critical uses. They are not suitable for medical or other high accuracy
* use cases.
*
* Accuracy is best between 1000K and 40000K.
*
* See http://github.com/neilbartlett/color-temperature for further details.
*
**/
using UnityEngine;
using System.Collections;

public class ColorTemp {

	/**
   * A more accurate version algorithm based on a different curve fit to the
   * original RGB to Kelvin data.
   * Input: color temperature in degrees Kelvin
   * Output: json object of red, green and blue components of the Kelvin temperature
   */
	public static Color ColorFromTemperature(float kelvin) {

		float temperature = kelvin / 100.0f;
		float red, green, blue;

		if (temperature < 66.0) {
			red = 255;
		} else {
			// a + b x + c Log[x] /.
			// {a -> 351.97690566805693`,
			// b -> 0.114206453784165`,
			// c -> -40.25366309332127
			//x -> (kelvin/100) - 55}
			red = temperature - 55.0f;
			red = 351.97690566805693f + 0.114206453784165f * red - 40.25366309332127f * Mathf.Log(red);
			if (red < 0) red = 0;
			if (red > 255) red = 255;
		}

		/* Calculate green */

		if (temperature < 66.0) {

			// a + b x + c Log[x] /.
			// {a -> -155.25485562709179`,
			// b -> -0.44596950469579133`,
			// c -> 104.49216199393888`,
			// x -> (kelvin/100) - 2}
			green = temperature - 2;
			green = -155.25485562709179f - 0.44596950469579133f * green + 104.49216199393888f * Mathf.Log(green);
			if (green < 0) green = 0;
			if (green > 255) green = 255;

		} else {

			// a + b x + c Log[x] /.
			// {a -> 325.4494125711974`,
			// b -> 0.07943456536662342`,
			// c -> -28.0852963507957`,
			// x -> (kelvin/100) - 50}
			green = temperature - 50.0f;
			green = 325.4494125711974f + 0.07943456536662342f * green - 28.0852963507957f * Mathf.Log(green);
			if (green < 0) green = 0;
			if (green > 255) green = 255;

		}

		/* Calculate blue */
		if (temperature >= 66.0) {
			blue = 255;
		} else {

			if (temperature <= 20.0) {
				blue = 0;
			} else {

				// a + b x + c Log[x] /.
				// {a -> -254.76935184120902`,
				// b -> 0.8274096064007395`,
				// c -> 115.67994401066147`,
				// x -> kelvin/100 - 10}
				blue = temperature - 10;
				blue = -254.76935184120902f + 0.8274096064007395f * blue + 115.67994401066147f * Mathf.Log(blue);
				if (blue < 0) blue = 0;
				if (blue > 255) blue = 255;
			}
		}

		return new Color(red / 255, green /255, blue / 255, 1f);
	}

	/**
   convert an rgb in JSON format into to a Kelvin color temperature
   */
	public static float ColorToTemperature(Color rgb) {
		float temperature = 0 ;
		Color testRGB;
		var epsilon = 0.4f;
		var minTemperature = 1000f;
		var maxTemperature = 40000f;
		while (maxTemperature - minTemperature > epsilon) {
			temperature = (maxTemperature + minTemperature) / 2;
			testRGB = ColorFromTemperature(temperature);
			if ((testRGB.b / testRGB.r) >= (rgb.b / rgb.r)) {
				maxTemperature = temperature;
			} else {
				minTemperature = temperature;
			}
		}
		return Mathf.Round(temperature);
	}

	/**
   * A JS verion of Tanner Helland's original algorithm.
   * Input: color temperature in degrees Kelvin
   * Output: json object of red, green and blue components of the Kelvin temperature
   */
   public static Color ColorTemperature2rgbOriginal(float kelvin) {
       
		float temperature = kelvin / 100.0f;
        
		float red, green, blue;
        
		if (temperature <= 66.0f) {
			red = 255;
		} else {
			red = temperature - 60.0f;
			red = 329.698727446f * Mathf.Pow(red, -0.1332047592f);
			if (red < 0) red = 0;
			if (red > 255) red = 255;
		}

		/* Calculate green */
		if (temperature <= 66.0f) {
			green = temperature;
			green = 99.4708025861f * Mathf.Log(green) - 161.1195681661f;
			if (green < 0) green = 0;
			if (green > 255) green = 255;
		} else {
			green = temperature - 60.0f;
			green = 288.1221695283f * Mathf.Pow(green, -0.0755148492f);
			if (green < 0) green = 0;
			if (green > 255) green = 255;
		}

		/* Calculate blue */

		if (temperature >= 66.0) {
			blue = 255;
		} else {

			if (temperature <= 19.0) {
				blue = 0;
			} else {
				blue = temperature - 10;
				blue = 138.5177312231f * Mathf.Log(blue) - 305.0447927307f;
				if (blue < 0) blue = 0;
				if (blue > 255) blue = 255;
			}
		}
		//Color expects values 0 to 1, so convert from 0-255
		return new Color(red / 255, green /255, blue / 255, 1f);
	}


}