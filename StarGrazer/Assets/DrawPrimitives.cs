﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class DrawPrimitives : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Calculates a point that is at an angle from the origin (0 is to the right)
	/// </summary>
	private Vector2 DegreesToXY(float degrees, float radius, Vector2 origin)
	{
		Vector2 xy = new Vector2();
		float radians = degrees * Mathf.PI / 180.0f;

		xy.x = Mathf.Cos(radians) * radius + origin.x;
		xy.y = Mathf.Sin(-radians) * radius + origin.y;

		return xy;
	}

	/// <summary>
	/// Calculates the angle a point is to the origin (0 is to the right)
	/// </summary>
	private float XYToDegrees(Vector2 xy, Vector2 origin)
	{
		float deltaX = origin.x - xy.x;
		float deltaY = origin.y - xy.y;

		float radAngle = Mathf.Atan2(deltaY, deltaX);
		float degreeAngle = radAngle * 180.0f / Mathf.PI;

		return (float)(180.0 - degreeAngle);
	}

	private Vector2[] CalculateVertices(int sides, int radius, int startingAngle, Vector2 center)
	{
//		if (sides < 3)
//			throw new ArgumentException("Polygon must have 3 sides or more.");
//
		List<Vector2> points = new List<Vector2>();
		float step = 360.0f / sides;

		float angle = startingAngle; //starting angle
		for (double i = startingAngle; i < startingAngle + 360.0; i += step) //go in a full circle
		{
			points.Add(DegreesToXY(angle, radius, center)); //code snippet from above
			angle += step;
		}

		return points.ToArray();
	}
}
