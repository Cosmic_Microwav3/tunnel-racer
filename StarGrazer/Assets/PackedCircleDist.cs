﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Circle primitive that wraps the Star object.
/// Mostly to maintain the original design of the circle packer,
/// probably could operate directly on Star gameobjects but sorta
/// liked the abstraction...
/// </summary>
class Circle{

	Star star;

	public Vector2 center{
		get {
			return new Vector2(star.transform.position.x, star.transform.position.y);
		}
		set {
			star.transform.position = new Vector3(value.x, value.y, 0);
		}
	}

	public float radius {
		get {
			return star.SphereOfInfluenceRadius;
		}
	}

	public Circle(Star star) {
		this.star = star;
	}    


	public override string ToString() {
		return "Rad: " + radius + " _ Center: " + center.ToString();

	}      
}

public class PackedCircleDist {

	List<Circle> circles = new List<Circle>();

	//TODO: could probably replace set number of iterations by measuring the
	//relative position change across iterations and stopping when it becomes small
	int iterLimit = 1000; //number of iterations to to perform packing over
	float minSeparation;
	Vector2 packingCenter;

	public PackedCircleDist() {
		packingCenter = new Vector2(0, 0);
		minSeparation = 1f;
	}
	public PackedCircleDist(Vector2 center, float minSep) {
		packingCenter = center;
		minSeparation = minSep;
	}

	public void Apply(List<Star> stars) {
		circles.Clear();
		//We create the stars with default position, so first want to distribute them randomly over a wide area...
		//Do this by summing star 2*SphereOfInfluenceRadius then distributing them randomly over
		//rect of size Sum(2*SOIRadius)
		//Note: can't use list sum since unity c# version is old...
		var sumRadii = 0f;

		foreach (var star in stars) {
			sumRadii += star.SphereOfInfluenceRadius;
		}

		foreach (var star in stars) {
			//We create the stars with default position, so first want to distribute them randomly over a wide area...
			var randPos = new Vector2(Random.Range(-sumRadii, sumRadii), Random.Range(-sumRadii, sumRadii));

			var circle = new Circle(star);
			circle.center = randPos;
			circles.Add(circle);
		}

		//Perform the packing with the cicles
		//IMPORTANT: since damping is divided by the iteration counter, need to 
		//start from 1 otherwise get div by zero problem
		int iterCount = 1;
		float motion = 1000000f; //arbitrary big number to start with

		while (motion > 1f) {
//		while (iterCount < 1000) {
			motion = UpdatePacking(iterCount);
			// Debug.Log(motion);
			iterCount += 1;
		}

//		for (int i = 1; i <= iterLimit; i++) {
//			UpdatePacking(i);
////			Debug.Log(stars);
//		}


	}

	/// <summary>
	/// Update the packing of the current circle list
	/// and return a measure of how far they all move
	/// towards the packing centre this time, to give an indication
	/// of whether there is still much change...
	/// </summary>
	private float UpdatePacking(int iterationCounter) {
		// Sort circles based on the distance to center
		circles.Sort(Comparer);

		for (int i = 0; i < circles.Count - 1; i++){
			for (int j = i + 1; j < circles.Count; j++){

				if (i == j)
					continue;

				Vector2 delta = circles[j].center - circles[i].center;

				float sumRadius = circles[i].radius + circles[j].radius;

				// Length squared = (dx * dx) + (dy * dy);
				//FIXME: not sure what this does, and if its doing what it's actually supposed to...
				float paddedDelta = delta.magnitude - minSeparation;
				float minSep = Mathf.Min(paddedDelta, minSeparation);

				paddedDelta -= minSep;

				if (paddedDelta < sumRadius){
					delta.Normalize();
					delta *= (sumRadius - paddedDelta) * 0.5f;

					circles[j].center += delta;
					circles[i].center -= delta;
				}
			}
		}

		float damping = 0.5f / iterationCounter;

		float totalMotion = 0f;

		for (int i = 0; i < circles.Count; i++){
			Vector2 toCentre = circles[i].center - this.packingCenter;
			toCentre *= damping;
			circles[i].center -= toCentre;

			totalMotion += toCentre.magnitude;
		}

		//Return how far everything moved this iteration
		//so the caller can decide whether to keep going...
		return totalMotion;
	}

	/// <summary>
	///
	/// </summary>
	/// <param name="?"></param>
	/// <returns></returns>
	private float DistanceToCenterSq(Circle circle) {
		return (circle.center - packingCenter).sqrMagnitude;
	}

	/// <summary>
	///
	/// </summary>
	private int Comparer(Circle p1, Circle P2) {
		float d1 = DistanceToCenterSq(p1);
		float d2 = DistanceToCenterSq(P2);

		if (d1 < d2)
			return 1;
		else if (d1 > d2)
			return -1;
		else return 0;
	}

}



