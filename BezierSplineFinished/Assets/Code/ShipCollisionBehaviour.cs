﻿using UnityEngine;
using System.Collections;

public class ShipCollisionBehaviour : MonoBehaviour {

	public float collisionSlowdown = 0.75f;
	public int numCollisions = 0;

	// Use this for initialization
	void OnCollisionEnter(Collision col){

		Destroy (col.gameObject);
		Ship._accel *= collisionSlowdown;
		numCollisions += 1;
	}
}
