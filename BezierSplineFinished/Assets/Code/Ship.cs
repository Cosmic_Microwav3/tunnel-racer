﻿using UnityEngine;
using System.Collections.Generic;
using String = System.String;

public class Ship : MonoBehaviour {


	public LineRenderer lr; //debugging please delete

	public float rotradius = 5f;
	
	//rotatespeed actually max speed
	public float maxrotateSpeed = 2f;
	public float rot_accel = 0.3f;
	public float initSpeed = .1f;
	public float accel =0.001f;
	public float curveboostfactor=0.001f;
	public float threshold = 0.1f; 
	public float radius = 10.0f;  
	public float speed = 20.0f;
	public static float _accel=1f;

	float curveRad;
	Vector3 curveCentre;
	float curveboost;
	//int num = 1;
	float rotateSpeed = 0;
	private Vector3 _curSplinePosition;

	//Some read-only properties

	private float _angle = 0f;

	public float rotationAngle {
		get {
			return _angle;
		}
	}

	//The current position on the path the ship follows
	public Vector3 splinePosition {
		get {
			return _curSplinePosition;
		}    
	}

	private float _fraction  = 3.0f/5.0f; 

//	public float fraction {
//		get {
//			return _fraction;
//		}
//	}

	private float speed_score;

	private float _speed; 
	private float boundary = 2.0f / 5.0f;


	public SplinePath splinePath;
	

	void Awake() {

	}


	private void Start(){
		_speed = speed / splinePath.GetPathLength();
	}

	private void Update () {

		_curSplinePosition = splinePath.PointOnPath(_fraction);

		transform.position = _curSplinePosition; //put it on the right position

		transform.LookAt(splinePath.PointOnPath(_fraction+0.01f)); //look ahead slightly

		var rotate = new Vector3 (Mathf.Sin(_angle), -Mathf.Cos (_angle), 0f);
		transform.Translate(rotate * rotradius);
		
		transform.Rotate(Vector3.forward, _angle * 180f/Mathf.PI);

		//find the curvature variables necessary
		splinePath.GetCurvature(_fraction, out curveCentre, out curveRad);

		//boost value given by the angle between the vector from the curvature, and the vector of the ship from the center
		//normalised by the magniture of the curvature
		//Very messy, needs cleaning.

		//angle between best angle, and current angle. -180f since the vector takes the inside line
		curveboost = Mathf.Cos ((Vector3.Angle (curveCentre - _curSplinePosition, transform.position - _curSplinePosition) - 180f)*Mathf.PI/180f)/curveRad; 

//		print (curveboost);

		//de-bugging, please delete
//		lr.SetVertexCount(2);
//		for (int i = 0; i < 2; i++) {
//			if (i==0){
//				lr.SetPosition(i, _curSplinePosition);
//			}
//			if (i==1){
//				lr.SetPosition(i, (curveCentre));
//			}
//		}

		if(Input.GetKey(KeyCode.LeftArrow)){
			if (rotateSpeed >- maxrotateSpeed){
				rotateSpeed -= rot_accel;
			}
		}
		if(Input.GetKey(KeyCode.RightArrow)){
			if (rotateSpeed < maxrotateSpeed){
				rotateSpeed += rot_accel;
			}
		}



		_angle += rotateSpeed * Time.deltaTime;
		// prevent angle from growing indefinitely...
		if (_angle >= 2*Mathf.PI ){
			_angle = _angle - 2*Mathf.PI;
		}
		 
		_fraction += Time.deltaTime * _speed * _accel;

		speed_score = speed * _accel;
		//give an acceleration based on the curvature
		_accel += accel + curveboostfactor*curveboost;

		SpeedManager.speed = speed_score;
		if (speed_score > ScoreManager.score) {
			ScoreManager.score = speed_score;
		}

		if (_fraction > boundary) {
			splinePath.AddRandomPathSegment();
			_fraction = 1.0f/5.0f;
			_speed = speed / splinePath.GetPathLength();
		} 
	}


}
