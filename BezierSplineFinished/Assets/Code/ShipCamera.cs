﻿using UnityEngine;
using System.Collections;

public class ShipCamera : MonoBehaviour {

	// The target we are following
	[SerializeField]
	private Ship target;

//	[SerializeField]
//	private Vector3 offset;

	[SerializeField]
	private float offsetBehind = 5.0f;

	[SerializeField]
	private float offsetAbove = 1.0f;

	[SerializeField]
	private float rotationDamping = 100;

	// Use this for initialization
	void Start() { 
	
	}
	
	// Update is called once per frame
	void LateUpdate()
	{
		// Early out if we don't have a target
		if (!target)
			return;

		var offset = offsetAbove * (target.transform.position - target.splinePosition).normalized + target.transform.forward.normalized*offsetBehind;

//		var _angle = target.rotationAngle;

//		var rotate = new Vector3 (Mathf.Sin(_angle), -Mathf.Cos (_angle), 0f);


		transform.position = target.transform.position - offset;

		var step = rotationDamping * Time.deltaTime;
		transform.localRotation = Quaternion.RotateTowards(transform.localRotation, target.transform.localRotation, step);

//		transform.localRotation = target.transform.localRotation;
//		// Calculate the current rotation angles
//		var wantedRotationAngle = target.eulerAngles.y;
//		var wantedHeight = target.position.y + height;
//		
//		var currentRotationAngle = transform.eulerAngles.y;
//		var currentHeight = transform.position.y;
//		
//		// Damp the rotation around the y-axis
//		currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
//		
//		// Damp the height
//		currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);
//		
//		// Convert the angle into a rotation
//		var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
//		
//		// Set the position of the camera on the x-z plane to:
//		// distance meters behind the target
//		transform.position = target.position;
//		transform.position -= currentRotation * Vector3.forward * distance;
//		
//		// Set the height of the camera
//		transform.position = new Vector3(transform.position.x ,currentHeight , transform.position.z);
//		
//		// Always look at the target
//		transform.LookAt(target);
	}

}
