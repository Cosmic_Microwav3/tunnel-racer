﻿Shader " Vertex Colored" {
//Properties {
//    _Color ("Main Color", Color) = (1,1,1,1)
//    _SpecColor ("Spec Color", Color) = (1,1,1,1)
//    _Emission ("Emmisive Color", Color) = (0,0,0,0)
//    _Shininess ("Shininess", Range (0.01, 1)) = 0.7
//    _MainTex ("Base (RGB)", 2D) = "white" {}
//}
//
//SubShader {
//    Pass {
//        Material {
//            Shininess [_Shininess]
//            Specular [_SpecColor]
//            Emission [_Emission]    
//        }
//        ColorMaterial AmbientAndDiffuse
//        Lighting On
//        SeperateSpecular On
//        SetTexture [_MainTex] {
//            Combine texture * primary, texture * primary
//        }
//        SetTexture [_MainTex] {
//            constantColor [_Color]
//            Combine previous * constant DOUBLE, previous * constant
//        } 
//    }
//}
//
//Fallback " VertexLit", 1
//}


  SubShader {
    Pass {
      CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag
      #include "UnityCG.cginc"
     
      struct v2f {
          float4 pos : SV_POSITION;
          fixed4 color : COLOR;
      };
      
      v2f vert (appdata_base v)
      {
          v2f o;
          o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
          o.color.xyz = v.normal * 0.5 + 0.5;
          o.color.w = 1.0;
          return o;
      }

      fixed4 frag (v2f i) : SV_Target { return i.color; }
      ENDCG
    }
  } 
}
