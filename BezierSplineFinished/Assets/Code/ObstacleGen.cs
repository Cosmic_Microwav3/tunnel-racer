﻿using UnityEngine;
//using System.Collections;
using System.Collections.Generic;

public class ObstacleGen : MonoBehaviour {
	
	public GameObject obstaclePrefab;
	public SplinePath splinePath;
	public float obstacleRadius;
	public float obstacleDensity = 0.5f;

	private Queue<Object> obstacleInstances = new Queue<Object>();

	void Awake(){
		SplinePath.OnPathGenerated += AddNewObstacles;

	}

	public void AddNewObstacles(){
		float startfrac =  3.0f/5.0f;
		float endfrac = 4.0f/5.0f;
		float step = 0.01f;
		Vector3 position = splinePath.PointOnPath(startfrac);

		for (float frac=startfrac+step;frac < endfrac; frac+=step){
			// Get current position
			position = splinePath.PointOnPath(frac);
			
			float objval = Random.Range (0f, 1f);
			if (objval < obstacleDensity) {
				AddObstacle(position, obstacleRadius);
			}
		}

		CleanupObstacles();
	}

	/**
	 * remove old cubes. At the moment just removes the oldest cube.
	 * Currently just call this every time we add a new cube, so we always have the same number
	 * 
	 **/
	public void CleanupObstacles(){
		while (obstacleInstances.Count > 50) {
			Object obj = obstacleInstances.Dequeue();
			Destroy(obj);
		}

	}

	public void AddObstacle(Vector3 pos, float radius ){


		float angle = Random.Range (0f, 2f * Mathf.PI);

		Vector3 rotate = new Vector3 (Mathf.Sin(angle), -Mathf.Cos (angle), 0f);

		Object clone = Instantiate(obstaclePrefab, pos + rotate * radius, Random.rotation); 

		obstacleInstances.Enqueue(clone);

	}


}
