﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using String = System.String;


[RequireComponent(typeof(MeshRenderer))]
public class Tunnel : MonoBehaviour {

	public SplinePath splinePath;
	public float radius = 6f; // Trail cross section width X

	public Queue<Mesh> meshes = new Queue<Mesh>();

	private int segmentCounter = 0;
	private int maxSegments = 3;
	private int oldestSegment = 0;
	public Queue<CombineInstance> combines = new Queue<CombineInstance>();

	public Queue<GameObject> segments = new Queue<GameObject>();

	TunnelBuilder builder;

	// Use this for initialization
	void Awake () {
		//Register function to call on add segment 
		SplinePath.OnPathGenerated += AddSegement;
		builder = new TunnelBuilder();
	}

	void Start() {
	}

	void AddSegement(){

		builder.BuildSegment(splinePath, radius);
		Mesh segmentMesh =  builder.CreateMesh();
		segmentMesh.RecalculateNormals();
//		NormalSolver.RecalculateNormals(segmentMesh, 0);



		GameObject segmentObject = new GameObject(String.Format("tunnelsection_{0}", segmentCounter));
		MeshFilter filter = segmentObject.AddComponent<MeshFilter>();
		filter.mesh = segmentMesh;

		MeshRenderer renderer = segmentObject.AddComponent<MeshRenderer>();

		var rend = GetComponent<MeshRenderer>();
		renderer.material = rend.material;

//		renderer.material = Resources.Load("Materials/TunnelMaterial") as Material;
		
		segmentObject.transform.parent = transform;

		segments.Enqueue(segmentObject);
			
		segmentCounter +=1;
		if (segments.Count > maxSegments) {

//		if (combines.Count > maxSegments) {
			Destroy(GameObject.Find(String.Format("tunnelsection_{0}", oldestSegment)));

			oldestSegment +=1;
			Destroy(segments.Dequeue());
//			combines.Dequeue();

		}


	}
}
