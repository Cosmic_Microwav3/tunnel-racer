﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SplinePath : MonoBehaviour {

	//Events so we can have game objects register functions to be called when the spline is extended
	public delegate void PathGeneratedAction();
	public static event PathGeneratedAction OnPathGenerated;

	public bool showLine = true;
	public LineRenderer lr; // set on game component
	
	//the following variables only work for 6 CPs...must change this...at some point
	private Transform[] markers = new Transform[6]; 
	private Vector3[] path = new Vector3[6]; 
	private float maxL=30.0f; //maximum length for cone

	

	// Use this for initialization
	void Awake () {
		for (int i = 0; i < path.Length; i++) { 
			if (i==0){
				path[0]=randomCone();
			}
			else{
				path[i]=path[i-1]+randomCone();
			}

			//path [i] = Random.insideUnitSphere * radius; 
			if (showLine){
				GameObject go = GameObject.CreatePrimitive (PrimitiveType.Cube);
				go.transform.localScale = new Vector3 (0.2f, 0.2f, 0.2f); 
				go.transform.position = path [i]; 
				markers[i] = go.transform;
			}
 
		}

		if (showLine){
			SetLineRenderer();
		}


	}

	void Start(){
		OnPathGenerated();

	}

//	@ContextMenu("Add Path Segment");
	public void AddRandomPathSegment(){
		// when new segment is added, replaces old spline control points, tho old line is left.
		for (int i = 0; i < path.Length-1; i++) {
			path[i] = path[i+1];
		}
		path[path.Length-1] += randomCone();

		if (showLine){
			SetLineRenderer();
			SetMarkers();
		}

		OnPathGenerated();
	}

	public float GetPathLength() {
		float length = 0.0f;
		for (var i = 1; i < path.Length; i++) {
			length += Vector3.Distance(path[i-1], path[i]);
		}
		return length;
	}

	public Vector3 PointOnPath(float f) {
		return iTween.PointOnPath(path, f);
	}
	
	private void SetMarkers() {
		for (var i = 0; i < markers.Length; i++) 
			markers[i].position = path[i];
	}
	
	private void SetLineRenderer() {
		lr.SetVertexCount(101);
		for (int i = 0; i < 101; i++) {
			float f = i / 100.0f;
			lr.SetPosition(i, iTween.PointOnPath(path, f));
		}
	}

	public float GetCurvature(float f, out Vector3 centre, out float radius) {
		//pick 3 points close to where the ship is

		//ToDo: comment this. It's mostly a black box at the moment.

		//choose 3 points close to the position of the ship
		Vector3 pointa = PointOnPath (f - 0.001f);
		Vector3 pointb = PointOnPath (f);
		Vector3 pointc = PointOnPath (f + 0.001f);


		//find the difference between points to the first one
		Vector3 diffba = pointb - pointa;
		Vector3 diffca = pointc - pointa;


		//normalise the vectors
		Vector3 diffba_norm = Vector3.Normalize (diffba);
		Vector3 diffca_norm = Vector3.Normalize (diffca);


		//will only need magnitude of one
		float ba_mag = Vector3.Magnitude (diffba);
		//float ca_mag = Vector3.Magnitude (diffca);

		//dot product of the normalised vectors
		float dotp = Vector3.Dot (diffca_norm, diffba_norm);

		//orthogonal vector of ca vector to ba
		Vector3 ca_orth = diffca_norm - dotp * diffba_norm;

		//normalised
		Vector3 ca_orth_norm = Vector3.Normalize(ca_orth);

		//Vector2 p1_2d = Vector2 (0f, 0f);
		//Vector2 p2_2d = Vector2 (ba_mag, 0f); 
		//Vector3 abc_perp = Vector3.Cross (diffba, diffca);


		//magic
		Vector2 p3_2d =Vector2.zero;
		for (int i=0; i<3; i++) {
			p3_2d [0] += diffca [i] * diffba_norm [i];
			p3_2d [1] += diffca [i] * ca_orth_norm [i];
		}

		float t = 0.5f * (ba_mag - p3_2d [0]) / p3_2d [1];

		float scale1 = p3_2d[0]/2f + p3_2d[1]*t;
		float scale2 =p3_2d[1]/2f - p3_2d[0] *t;

		centre = pointa + scale1 * diffba_norm + scale2 * ca_orth_norm;

		radius = Vector3.Magnitude (centre - pointa);


		//curveMagnitude= Vector3.Magnitude(diffdiff);

		return 0f;
	}

	
	
	private Vector3 randomCone(){
		//give a position inside a square maxL away of sides 2*maxL in the Z direction...I hope
		return new Vector3 (Random.Range(-maxL,maxL),Random.Range(-maxL,maxL),maxL);
	}


}
