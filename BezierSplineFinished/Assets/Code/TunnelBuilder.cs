﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TunnelBuilder : MeshBuilder {

	int segmentCount = 18;
	bool islight =false;
	float lightfreq=0.05f;

	List<Vector3> LastRing = null;
	List<Vector2> LastUVs = null;

	public List<Vector3> VerticesTemp = new List<Vector3>();
	protected List<Vector2> UVsTemp = new List<Vector2>();


	public void Reset(){
		LastRing = null;
	}

	public void BuildRingSharp(Vector3 centre, float radius, float v, Quaternion rotation, bool buildTriangles,bool makelight){
		
		float angleInc = (Mathf.PI * 2.0f) / segmentCount;
		float r;
		int vertsPerRow = segmentCount;
		
		int baseIndex = VerticesTemp.Count - 1;
		
		
		int index0;
		int index1;
		int index2;
		int index3;

		if (makelight) {
			//TODO: make sure this is a child of the mesh and gets removed at the same time

			GameObject lightGameObject = new GameObject("The Light");
			Light lightComp = lightGameObject.AddComponent<Light>();
			lightComp.intensity = 2f;
			lightComp.range = 30f;
			lightComp.bounceIntensity = 1f;

			//lightComp.color = Color.blue;
			lightGameObject.transform.position = centre+Vector3.up*(radius-0.1f*radius);

		}
		
		for (int i = 0; i < segmentCount; i++)
		{
			float angle = angleInc * i;
			
			var unitPosition = Vector3.zero;
			unitPosition.x = Mathf.Cos(angle);
			unitPosition.y = Mathf.Sin(angle);
			
			unitPosition = rotation * unitPosition;
			
			r = radius + Random.Range(0f, 0.5f * radius);
			VerticesTemp.Add(centre + unitPosition * r);
//			Normals.Add(unitPosition);
			UVsTemp.Add(new Vector2((float)i / segmentCount, v));
			//Always builds a quad (2 tris) from the last 2 verts created and 2 verts from previously created ring (idx-vertsPerRow)
			if (i > 0 && buildTriangles)
			{
				baseIndex = VerticesTemp.Count - 1;
				
				
				index0 = baseIndex;
				index1 = baseIndex - 1;
				index2 = baseIndex - vertsPerRow;
				index3 = baseIndex - vertsPerRow - 1;
				
				AddTriangleAsCopy(index0, index2, index1);
				AddTriangleAsCopy(index2, index3, index1);
			}
		}
		// Close the ring by joining the first and last segments
		// need 2 last verts and 2 verts from start of ring
		if (buildTriangles) {
			baseIndex = VerticesTemp.Count - 1;
			//			Debug.Log(baseIndex);
			
			index0 = baseIndex - vertsPerRow + 1;
			index1 = baseIndex ;
			
			index2 = baseIndex - vertsPerRow - vertsPerRow + 1;
			index3 = baseIndex - vertsPerRow;

			
			AddTriangleAsCopy(index0, index2, index1);
			AddTriangleAsCopy(index2, index3, index1);
		}
	}

	public void AddTriangleAsCopy(int index0, int index1, int index2)
	{
		int i = Vertices.Count;
		Vertices.Add(VerticesTemp[index0]); //this will now be the ith element
		Vertices.Add(VerticesTemp[index1]);
		Vertices.Add(VerticesTemp[index2]);

		// Need to add UVs to match added verts
		// Use same idea as verts - keep 'linear' temp version, then re-add UVs corresponding to duplicated verts
		UVs.Add(UVsTemp[index0]);
		UVs.Add(UVsTemp[index1]);
		UVs.Add(UVsTemp[index2]);

		Indices.Add(i);
		Indices.Add(i+1);
		Indices.Add(i+2);
	}

	/// <summary>
	/// Builds a ring as part of a cylinder.
	/// </summary>
	/// <param name="meshBuilder">The mesh builder currently being added to.</param>
	/// <param name="segmentCount">The number of segments in this ring.</param>
	/// <param name="centre">The position at the centre of the ring.</param>
	/// <param name="radius">The radius of the ring.</param>
	/// <param name="v">The V coordinate for this ring.</param>
	/// <param name="buildTriangles">Should triangles be built for this ring? This value should be false if this is the first ring in the cylinder.</param>
	/// <param name="rotation">A rotation value to be applied to the whole ring.</param>
	public void BuildRing(Vector3 centre, float radius, float v, Quaternion rotation, bool buildTriangles)
	{
		float angleInc = (Mathf.PI * 2.0f) / segmentCount;
		float r;
		int vertsPerRow = segmentCount;

		int baseIndex = Vertices.Count - 1;
		
		
		int index0;
		int index1;
		int index2;
		int index3;

		for (int i = 0; i < segmentCount; i++)
		{
			float angle = angleInc * i;
			
			Vector3 unitPosition = Vector3.zero;
			unitPosition.x = Mathf.Cos(angle);
			unitPosition.y = Mathf.Sin(angle);
			
			unitPosition = rotation * unitPosition;

			r = radius + Random.Range(0,0.5f*radius);
			Vertices.Add(centre + unitPosition * r);
			Normals.Add(unitPosition);
			UVs.Add(new Vector2((float)i / segmentCount, v));

			//Always builds a quad (2 tris) from the last 2 verts created and 2 verts from previously created ring (idx-vertsPerRow)
			if (i > 0 && buildTriangles)
			{
				baseIndex = Vertices.Count - 1;
				

				 index0 = baseIndex;
				 index1 = baseIndex - 1;
				 index2 = baseIndex - vertsPerRow;
				 index3 = baseIndex - vertsPerRow - 1;
				
				AddTriangle(index0, index2, index1);
				AddTriangle(index2, index3, index1);
			}
		}
		// Close the ring by joining the first and last segments
		// need 2 last verts and 2 verts from start of ring
		if (buildTriangles) {
			baseIndex = Vertices.Count - 1;
//			Debug.Log(baseIndex);

			index0 = baseIndex - vertsPerRow + 1;
			index1 = baseIndex ;

			index2 = baseIndex - vertsPerRow - vertsPerRow + 1;
			index3 = baseIndex - vertsPerRow;
			
			AddTriangle(index0, index2, index1);
			AddTriangle(index2, index3, index1);
		}

	}
	
	public void BuildSegment(SplinePath splinePath, float tunnelRadius) {
		BuildSegment(splinePath, tunnelRadius, 3.0f/5.0f, 4.0f/5.0f);
	}

	public void BuildSegment(SplinePath splinePath, float tunnelRadius, float startfrac, float endfrac) {
		m_Vertices.Clear();
		VerticesTemp.Clear();

		UVsTemp.Clear();
		UVs.Clear();
		
		Indices.Clear();

		var position = splinePath.PointOnPath(startfrac);

		Vector3 tempPos = splinePath.PointOnPath(startfrac+0.001f); 
		//ideally we want the tangent of the curve at the starting point :/
		//make do with just a small delta...

		Vector3 direction = tempPos - position;


		var rotation = Quaternion.FromToRotation(Vector3.forward, direction);
		//build the first ring(with no tris), or use the one from the last created segment

		if (LastRing == null) {
			BuildRingSharp(position, tunnelRadius, startfrac, rotation, false, islight);
		}
		else {
			VerticesTemp.AddRange(LastRing);
			UVsTemp.AddRange(LastUVs);
		}


		var prevPosition = position;


		for (float frac=startfrac+0.01f; frac < endfrac; frac+=0.01f){
			// Get current position
			islight=false;
			position = splinePath.PointOnPath(frac);

			// Get vector pointing along path (from start to end position)
			direction = position - prevPosition;
			
			rotation = Quaternion.FromToRotation(Vector3.forward, direction);

			if (frac % lightfreq < 0.01f){
				islight=true;
			}

			BuildRingSharp(position, tunnelRadius, frac, rotation, true,islight);
			prevPosition = position;
		}
		//Set the last ring to the last created verticies
		LastRing = VerticesTemp.GetRange(VerticesTemp.Count - segmentCount-1, segmentCount+1);
		LastUVs = UVsTemp.GetRange(0, segmentCount+1);



	}
}
